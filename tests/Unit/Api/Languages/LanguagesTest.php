<?php

namespace Tests\Unit\Api\Languages;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Sanctum\Sanctum;
use App\User;
use App\Services\Languages\LanguagesService;

class LanguagesTest extends TestCase
{
	use RefreshDatabase;

	public function setUp(): void
	{
		parent::setUp();
		Sanctum::actingAs(factory(User::class)->create(['email' => 'jhon_doe@testing.com']), ['*']);
		$this->mock(LanguagesService::class, function ($mock) {
			$mock->shouldReceive('list')->once()
				->andReturn(['english', 'german']);
		});
	}

    public function testUserCanReceiveListOfLanguages()
    {
        $this->getJson('api/languages')
        	->assertStatus(200)
        	->assertSee('english')
        	->assertSee('german');
    }
}
