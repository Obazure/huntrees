<?php

namespace Tests\Unit\Api\Candidates;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Sanctum\Sanctum;
use App\User;

class CandidateStatusTest extends TestCase
{
    use RefreshDatabase;

	public function setUp(): void
	{
		parent::setUp();
		$this->seed();
		Sanctum::actingAs(factory(User::class)->create([
			'email' => 'jhon_doe@testing.com'
		]), ['*']);
	}

	public function testStatusListCanBeRetrieved()
	{
		$this->getJson('/api/candidate-statuses')->assertStatus(200)
			->assertJsonStructure([
				'*' => [
					'name',
					'serial',
					'desc',
				],
			])
			->assertSee('initial')
			->assertSee('cv-received')
			->assertSee('interview-passed')
			->assertSee('verified');
	}

	public function testStatusCanBeStored()
	{
		$this->postJson('api/candidate-statuses', [
			'name' => 'additional',
			'serial' => '5',
		])->assertStatus(201);

		$this->getJson('api/candidate-statuses')
			->assertSee('additional')
			->assertSee('5');
	}

	public function testStatusCanBeRetrievedByName()
	{
		$this->getJson('api/candidate-statuses/initial')
			->assertStatus(200)
			->assertJsonPath('name', 'initial');
	}

	public function testStatusCanBeUpdatedUsingName()
	{
		$this->patchJson('api/candidate-statuses/initial', [
			'name' => 'other',
		])->assertStatus(200);

		$this->getJson('api/candidate-statuses/other')
			->assertStatus(200);
	}

	public function testStatusCanBeDeletedUsingName()
	{
		$this->deleteJson('api/candidate-statuses/initial')
			->assertStatus(200);

		$this->getJson('api/candidate-statuses/initial')
			->assertStatus(404);
	}
}
