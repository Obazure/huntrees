<?php

namespace Tests\Unit\Api\Candidates;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Sanctum\Sanctum;
use App\User;
use App\Entities\Candidates\Candidate;

class CandidatesTest extends TestCase
{
    use RefreshDatabase;

	public function setUp(): void
	{
		parent::setUp();
		$this->seed();
		Sanctum::actingAs(factory(User::class)->create([
			'email' => 'jhon_doe@testing.com'
		]), ['*']);
	}

	public function testCandidateListCanBeRetrieved()
	{
		factory(Candidate::class)->states('male', 'full')->create([
			'name' => 'Jhon',
			'surname' => 'Doe',
		]);

		$this->getJson('api/candidates')->assertStatus(200)
			->assertJsonPath('data.0.name', 'jhon')
			->assertJsonPath('data.0.surname', 'doe')
			->assertJsonStructure([
				'meta' => [
					'total',
					'current_page',
					'per_page',
				],
				'data' => [
					'*' => [
						'id',
						'name',
						'surname',
						'gender',
						'status',
					],
				],
			]);
	}

	public function testCandidateCanBeStored()
	{
		$this->postJson('api/candidates', [
			'name' => 'Jhon',
			'surname' => 'Doe',
			'gender' => 'male',
		])
			->assertStatus(201)
			->assertJsonStructure([
				'data' => [
					'id',
					'name',
					'surname',
					'gender',
					'status',
				],
			]);
	}

	public function testCandidateCanBeRetrievedById()
	{
		$id = factory(Candidate::class)->create()->id;

		$this->getJson("api/candidates/$id")->assertStatus(200)
			->assertJsonPath('data.id', $id)
			->assertJsonStructure([
				'data' => [
					'id',
					'name',
					'surname',
					'gender',
					'status',
				],
			]);
	}

	public function testCandidateCanBeUpdatedById()
	{
		$id = factory(Candidate::class)->state('female')->create()->id;

		$this->patchJson("api/candidates/$id", [
			'name' => 'Jhon',
			'surname' => 'Doe',
			'gender' => 'male',
		])->assertStatus(200);

		$this->getJson("api/candidates/$id")
			->assertJsonPath('data.id', $id)
			->assertJsonPath('data.name', 'jhon')
			->assertJsonPath('data.surname', 'doe')
			->assertJsonPath('data.gender', 'male');
	}

	public function testCandidateCanBeDeletedById()
	{
		$id = factory(Candidate::class)->state('female')->create()->id;

		$this->deleteJson("api/candidates/$id")->assertStatus(200);
		$this->getJson("api/candidates/$id")->assertStatus(404);
	}
}
