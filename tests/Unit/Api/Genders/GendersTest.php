<?php

namespace Tests\Unit\Api\Genders;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Sanctum\Sanctum;
use App\User;
use App\Services\Genders\GendersService;

class GendersTest extends TestCase
{
	use RefreshDatabase;

	public function setUp(): void
	{
		parent::setUp();
		Sanctum::actingAs(factory(User::class)->create(['email' => 'jhon_doe@testing.com']), ['*']);
		$this->mock(GendersService::class, function ($mock) {
			$mock->shouldReceive('list')->once()
				->andReturn(['male', 'female']);
		});
	}

    public function testUserCanReceiveListOfGenders()
    {
        $this->getJson('api/genders')
        	->assertStatus(200)
        	->assertSee('male')
        	->assertSee('female');
    }
}
