<?php

namespace Tests\Unit\Api\ContactTypes;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Sanctum\Sanctum;
use App\User;
use App\Services\ContactTypes\ContactTypesService;

class ContactTypesTest extends TestCase
{
    use RefreshDatabase;

	public function setUp(): void
	{
		parent::setUp();
		Sanctum::actingAs(factory(User::class)->create(['email' => 'jhon_doe@testing.com']), ['*']);
		$this->mock(ContactTypesService::class, function ($mock) {
			$mock->shouldReceive('list')->once()
				->andReturn(['email', 'phone']);
		});
	}

    public function testUserCanReceiveListOfContactTypes()
    {
        $this->getJson('api/contact-types')
        	->assertStatus(200)
        	->assertSee('email')
        	->assertSee('phone');
    }
}
