<?php

namespace Tests\Unit\Api\Countries;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Sanctum\Sanctum;
use App\User;
use App\Services\Countries\CountriesService;

class CountriesTest extends TestCase
{
	use RefreshDatabase;

	public function setUp(): void
	{
		parent::setUp();
		Sanctum::actingAs(factory(User::class)->create(['email' => 'jhon_doe@testing.com']), ['*']);
	}

    public function testUserCanReceiveListOfCountries()
    {
    	$this->mock(CountriesService::class, function ($mock) {
			$mock->shouldReceive('list')->once()
				->andReturn(['usa', 'germany']);
		});

        $this->getJson('api/countries')
        	->assertStatus(200)
        	->assertSee('usa')
        	->assertSee('germany');
    }

    public function testUserCanReceiveListOfCitizenships()
    {
    	$this->mock(CountriesService::class, function ($mock) {
			$mock->shouldReceive('citizenships')->once()
				->andReturn(['american', 'german']);
		});

        $this->getJson('api/citizenships')
        	->assertStatus(200)
        	->assertSee('american')
        	->assertSee('german');
    }
}
