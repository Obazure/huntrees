<?php

namespace Tests\Unit\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Sanctum\Sanctum;
use App\User;

class AuthTest extends TestCase
{
	use RefreshDatabase;

    public function testAuthMiddlewareIsWorking()
    {
    	$this->getJson('api/user')->assertStatus(401);

    	Sanctum::actingAs(factory(User::class)->create(['email' => 'jhon_doe@testing.com']), ['*']);
    	
        $this->getJson('api/user')->assertStatus(200)
        	->assertSee('jhon_doe@testing.com');
    }
}
