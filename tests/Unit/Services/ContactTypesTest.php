<?php

namespace Tests\Unit\Services;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use ContactTypes;

class ContactTypesTest extends TestCase
{
	use RefreshDatabase;

	public function setUp(): void
	{
		parent::setUp();
		$this->seed();
	}

    public function testContactTypesListCanBeObtained()
    {
    	$types = collect(ContactTypes::list());
        $this->assertTrue($types->contains('email'));
        $this->assertTrue($types->contains('phone'));
    }
}
