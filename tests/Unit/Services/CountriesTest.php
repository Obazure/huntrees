<?php

namespace Tests\Unit\Services;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Countries;

class CountriesTest extends TestCase
{
	use RefreshDatabase;

	public function setUp(): void
	{
		parent::setUp();
		$this->seed();
	}

    public function testCountriesListCanBeObtained()
    {
    	$countries = collect(Countries::list());
        $this->assertTrue($countries->contains('usa'));
        $this->assertTrue($countries->contains('germany'));
    }

    public function testCitizenshipsListCanBeObtained()
    {
    	$citizenships = collect(Countries::citizenships());
    	$this->assertTrue($citizenships->contains('american'));
    	$this->assertTrue($citizenships->contains('german'));
    }
}
