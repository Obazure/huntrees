<?php

namespace Tests\Unit\Services;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Genders;

class GendersTest extends TestCase
{
	use RefreshDatabase;

	public function setUp(): void
	{
		parent::setUp();
		$this->seed();
	}

    public function testGendersListCanBeObtained()
    {
    	$genders = collect(Genders::list());
        $this->assertTrue($genders->contains('male'));
        $this->assertTrue($genders->contains('female'));
    }
}
