<?php

namespace Tests\Unit\Services;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Languages;

class LanguagesTest extends TestCase
{
	use RefreshDatabase;

	public function setUp(): void
	{
		parent::setUp();
		$this->seed();
	}

    public function testLanguagesListCanBeObtained()
    {
    	$languages = collect(Languages::list());
    	$this->assertTrue($languages->contains('english'));
    	$this->assertTrue($languages->contains('german'));
    }
}
