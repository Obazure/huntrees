### Local Deployment

```
$ cp .env.example .env
$ docker-compose up -d
$ docker-compose exec app /bin/sh -c " \
	&& composer install \
	&& php artisan key:generate \
	&& php artisan migrate:fresh --seed \
	&& php artisan migrate:fresh --seed --env=testing \
	&& php artisan test"
$ docker run -v $PWD:/home/node node:alpine /bin/sh -c "cd /home/node \
	&& yarn install \
	&& yarn run dev"
```
