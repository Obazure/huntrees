<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('Api')->middleware('auth:sanctum')->group(function () {
	Route::get('/user', function (Request $request) {
    	return $request->user();
	});

	Route::get('/users', 'UserController@index');
	Route::get('/users/{user}', 'UserController@show');

	// ------------------------------------
	// 		   General Dictionaries
	// ------------------------------------
	Route::namespace('Dictionaries')->group(function () {
		Route::get('languages', 'LanguageController@index');
		Route::get('genders', 'GenderController@index');
		Route::get('countries', 'CountryController@index');
		Route::get('citizenships', 'CitizenshipController@index');
		Route::get('contact-types', 'ContactTypeController@index');
	});

	// ------------------------------------
	// 		   	  Core Resources
	// ------------------------------------
	Route::namespace('Candidates')->group(function () {
		Route::apiResource('candidate-statuses', 'CandidateStatusController');
		Route::apiResource('candidates', 'CandidateController');
		Route::apiResource('candidates.comments', 'CandidateCommentsController');
	});
});
