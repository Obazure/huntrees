FROM php:7.4-fpm-alpine
RUN apk add --no-cache \
	composer \
	postgresql-dev \
	&& docker-php-ext-install pdo pdo_pgsql