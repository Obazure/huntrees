<?php

namespace App\Entities\Candidates;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class CandidateStatus extends Model
{
    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'name';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'serial', 'desc'];

    /**
     * Normalizes the inputed name
     */
    public function setNameAttribute($value)
    {
    	$value = Str::slug($value);
    	$this->attributes['name'] = $value;
    }

}
