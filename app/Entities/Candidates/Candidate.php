<?php

namespace App\Entities\Candidates;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use App\Traits\HasComments;

class Candidate extends Model
{
	use HasComments;
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'status',
    	'title',
    	'name',
    	'surname',
    	'gender',
    	'citizenship',
    	'residence',
    	'birthday',
    ];

    /**
	 * The attributes that should be cast.
	 *
	 * @var array
	 */
	protected $casts = [
	    'birthday' => 'datetime:Y-m-d',
	    'created_at' => 'datetime:Y-m-d',
	    'updated_at' => 'datetime:Y-m-d',
	];

	/**
	 * Formats inputed status
	 * 
	 * @param string $value
	 * @return void
	 */
	public function setStatusAttribute($value)
	{
		$this->attributes['status'] = Str::slug($value);
	}

	/**
	 * Formats inputed title
	 * 
	 * @param string $value
	 * @return void
	 */
	public function setTitleAttribute($value)
	{
		$this->attributes['title'] = Str::slug($value);
	}

	/**
	 * Formats inputed name
	 * 
	 * @param string $value
	 * @return void
	 */
	public function setNameAttribute($value)
	{
		$this->attributes['name'] = Str::slug($value);
	}

	/**
	 * Formats inputed surname
	 * 
	 * @param string $value
	 * @return void
	 */
	public function setSurnameAttribute($value)
	{
		$this->attributes['surname'] = Str::slug($value);
	}

	/**
	 * Formats inputed gender
	 * 
	 * @param string $value
	 * @return void
	 */
	public function setGenderAttribute($value)
	{
		$this->attributes['gender'] = Str::slug($value);
	}

	/**
	 * Formats inputed citizenship
	 * 
	 * @param string $value
	 * @return void
	 */
	public function setCitizenshipAttribute($value)
	{
		$this->attributes['citizenship'] = Str::slug($value);
	}

	/**
	 * Formats inputed residence
	 * 
	 * @param string $value
	 * @return void
	 */
	public function setResidenceAttribute($value)
	{
		$value = preg_replace('/\s+/', ' ', $value);
		$this->attributes['residence'] = trim($value);
	}
}
