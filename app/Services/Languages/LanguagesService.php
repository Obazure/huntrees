<?php

namespace App\Services\Languages;

use Illuminate\Support\Facades\DB;

class LanguagesService
{
    public function list()
    {
    	return DB::table('d_languages')->pluck('name')->toArray();
    }
}
