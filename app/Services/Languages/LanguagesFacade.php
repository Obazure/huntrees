<?php

namespace App\Services\Languages;

use Illuminate\Support\Facades\Facade;

class LanguagesFacade extends Facade
{
	protected static function getFacadeAccessor()
	{
		return LanguagesService::class;
	}
}