<?php

namespace App\Services\Countries;

use Illuminate\Support\Facades\Facade;

class CountriesFacade extends Facade
{
    protected static function getFacadeAccessor()
	{
		return CountriesService::class;
	}
}