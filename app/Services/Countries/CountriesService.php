<?php

namespace App\Services\Countries;

use Illuminate\Support\Facades\DB;

class CountriesService
{
    public function list()
    {
    	return DB::table('d_countries')->pluck('name')->toArray();
    }

    public function citizenships()
    {
    	return DB::table('d_countries')->pluck('citizenship')->toArray();
    }
}
