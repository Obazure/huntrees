<?php

namespace App\Services\Genders;

use Illuminate\Support\Facades\DB;

class GendersService
{
    public function list()
    {
    	return DB::table('d_genders')->pluck('name')->toArray();
    }
}
