<?php

namespace App\Services\Genders;

use Illuminate\Support\Facades\Facade;

class GendersFacade extends Facade
{
    protected static function getFacadeAccessor()
	{
		return GendersService::class;
	}
}
