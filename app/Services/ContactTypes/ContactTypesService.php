<?php

namespace App\Services\ContactTypes;

use Illuminate\Support\Facades\DB;

class ContactTypesService
{
    public function list()
    {
    	return DB::table('d_contact_types')->pluck('name')->toArray();
    }
}
