<?php

namespace App\Services\ContactTypes;

use Illuminate\Support\Facades\Facade;

class ContactTypesFacade extends Facade
{
    protected static function getFacadeAccessor()
	{
		return ContactTypesService::class;
	}
}
