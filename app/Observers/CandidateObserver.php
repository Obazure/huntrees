<?php

namespace App\Observers;

use App\Entities\Candidates\Candidate;
use Illuminate\Support\Facades\Auth;

class CandidateObserver
{
    /**
     * Handle the candidate "created" event.
     *
     * @param  \App\Entities\Candidate\Candidate  $candidate
     * @return void
     */
    public function created(Candidate $candidate)
    {
        $user = Auth::user();

        $candidate->comments()->create([
            'user_id' => $user->id,
            'type' => 'system',
            'message' => "Created by <b>{$user->name}</b>",
        ]);

        $candidate->comments()->create([
            'user_id' => $user->id,
            'type' => 'tech',
            'message' => $candidate->toJson(),
        ]);
    }

    /**
     * Handle the candidate "updated" event.
     *
     * @param  \App\Entities\Candidate\Candidate  $candidate
     * @return void
     */
    public function updated(Candidate $candidate)
    {
        $user = Auth::user();

        $updates = collect($candidate->getChanges());

        if ($updates->has('status')) {
            $candidate->comments()->create([
                'user_id' => $user->id,
                'type' => 'system',
                'message' => "User: <b>{$user->name}</b> changed status to <b>{$updates->get('status')}</b>",
            ]);
        }

        $candidate->comments()->create([
            'user_id' => $user->id,
            'type' => 'tech',
            'message' =>  $updates->forget('updated_at')->toJson(),
        ]);
    }

    /**
     * Handle the candidate "deleted" event.
     *
     * @param  \App\Entities\Candidate\Candidate  $candidate
     * @return void
     */
    public function deleted(Candidate $candidate)
    {
        $candidate->comments()->create([
            'user_id' => Auth::id(),
            'type' => 'tech',
            'message' => 'Candidate deleted',
        ]);
    }
}
