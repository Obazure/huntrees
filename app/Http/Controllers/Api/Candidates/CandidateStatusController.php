<?php

namespace App\Http\Controllers\Api\Candidates;

use App\Entities\Candidates\CandidateStatus;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;

class CandidateStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return CandidateStatus::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'max:16', Rule::unique(CandidateStatus::class, 'name')],
            'serial' => ['required', 'numeric', 'integer', Rule::unique(CandidateStatus::class, 'serial')],
        ]);

        return CandidateStatus::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entities\Candidates\CandidateStatus  $candidateStatus
     * @return \Illuminate\Http\Response
     */
    public function show(CandidateStatus $candidateStatus)
    {
        return $candidateStatus;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entities\Candidates\CandidateStatus  $candidateStatus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CandidateStatus $candidateStatus)
    {
        $request->validate([
            'name' => ['max:16', Rule::unique(CandidateStatus::class, 'name')->ignore($candidateStatus->name, 'name')],
            'serial' => ['numeric|integer', Rule::unique(CandidateStatus::class, 'serial')->ignore($candidateStatus->serial, 'serial')],
        ]);
        $candidateStatus->fill($request->all())->save();
        return $candidateStatus;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entities\Candidates\CandidateStatus  $candidateStatus
     * @return \Illuminate\Http\Response
     */
    public function destroy(CandidateStatus $candidateStatus)
    {
        return $candidateStatus->delete();
    }
}
