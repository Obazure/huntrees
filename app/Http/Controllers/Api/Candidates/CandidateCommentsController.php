<?php

namespace App\Http\Controllers\Api\Candidates;

use App\Comment;
use App\Entities\Candidates\Candidate;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class CandidateCommentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Candidate $candidate)
    {
        return $candidate->comments()->where('type', '<>', 'tech')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Candidate $candidate)
    {
        $request->validate([
            'message' => 'required',
        ]);

        return $candidate->comments()->create([
            'user_id' => Auth::user()->id,
            'message' => $request->message,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $comment)
    {
        return $comment;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Candidate $candidate, Comment $comment)
    {
        $request->validate([
            'message' => 'required',
        ]);

        if (!Auth::id() === $comment->id) abort(403);

        $comment->fill(['message' => $request->message])->save();
        return $comment->refresh();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Candidate $candidate, Comment $comment)
    {
        if (!Auth::id() === $comment->id) abort(403);
        return $comment->delete();
    }
}
