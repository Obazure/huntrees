<?php

namespace App\Http\Controllers\Api\Candidates;

use App\Entities\Candidates\Candidate;
use App\Entities\Candidates\CandidateStatus;
use App\Http\Resources\CandidateResource;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use Genders;
use Countries;

class CandidateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->validate([
            'status' => 'exists:candidate_statuses,name',
        ]);

        $candidates = Candidate::
            when($request->has('status'), function ($q) use ($request) {
                $q->where('status', $request->status);
            })
            ->get();

        return CandidateResource::collection($candidates);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'gender' => [Rule::in(Genders::list())],
            'birthday' => 'date',
            'citizenship' => [Rule::in(Countries::citizenships())],
        ]);

        $candidate = Candidate::create(collect($request->all())
            ->put('status', CandidateStatus::first()->name)->toArray());
        return new CandidateResource($candidate);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entities\Candidates\Candidate  $candidate
     * @return \Illuminate\Http\Response
     */
    public function show(Candidate $candidate)
    {
        return new CandidateResource($candidate);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entities\Candidates\Candidate  $candidate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Candidate $candidate)
    {
        $request->validate([
            'status' => [Rule::exists(CandidateStatus::class, 'name')],
            'gender' => [Rule::in(Genders::list())],
            'birthday' => 'date',
            'citizenship' => [Rule::in(Countries::citizenships())],
        ]);
        
        $candidate->fill($request->all())->save();
        return new CandidateResource($candidate);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entities\Candidate  $candidate
     * @return \Illuminate\Http\Response
     */
    public function destroy(Candidate $candidate)
    {
        return $candidate->delete();
    }
}
