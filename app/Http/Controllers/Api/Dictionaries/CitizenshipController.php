<?php

namespace App\Http\Controllers\Api\Dictionaries;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Countries;

class CitizenshipController extends Controller
{
    public function index()
    {
    	return Countries::citizenships();
    }
}
