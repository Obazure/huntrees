<?php

namespace App\Http\Controllers\Api\Dictionaries;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Languages;

class LanguageController extends Controller
{
    public function index()
    {
    	return Languages::list();
    }
}
