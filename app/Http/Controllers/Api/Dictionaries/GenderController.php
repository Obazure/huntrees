<?php

namespace App\Http\Controllers\Api\Dictionaries;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Genders;

class GenderController extends Controller
{
	public function index()
	{
		return Genders::list();
	}
}
