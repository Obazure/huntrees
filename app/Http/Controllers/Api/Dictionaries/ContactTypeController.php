<?php

namespace App\Http\Controllers\Api\Dictionaries;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use ContactTypes;

class ContactTypeController extends Controller
{
    public function index()
    {
    	return ContactTypes::list();
    }
}
