<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\Carbon;

class CandidateResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'status' => $this->status,
            'title' => $this->title,
            'name' => $this->name,
            'surname' => $this->surname,
            'gender' => $this->gender,
            'birthday' => $this->birthday,
            'citizenship' => $this->citizenship,
            'residence' => $this->residence,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
