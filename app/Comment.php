<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Comment extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['user_id', 'type', 'commentable_id', 'commentable_type', 'message'];

	public function author()
	{
		return $this->belongsTo(User::class);
	}

	public function commentable()
	{
		return $this->morphTo();
	}
}
