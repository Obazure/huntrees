<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Entities\Candidates\Candidate;
use App\Entities\Candidates\CandidateStatus;
use Faker\Generator as Faker;

$factory->define(Candidate::class, function (Faker $faker) {
	$status = CandidateStatus::orderBy('serial', 'asc')->first()->name;
	$gender = collect(['male', 'female'])->random();
    return [
    	'status' => $status,
    	'name' => $faker->firstName($gender),
    	'surname' => $faker->lastName(),
        'gender' => $gender,
    ];
})
->state(Candidate::class, 'male', function ($faker) {
	return [
		'name' => $faker->firstName('male'),
		'gender' => 'male',
	];
})
->state(Candidate::class, 'female', function ($faker) {
	return [
		'name' => $faker->firstName('female'),
		'gender' => 'female',
	];
})
->state(Candidate::class, 'with_birthday', function ($faker) {
	return [
		'birthday' => $faker->dateTimeBetween('-50 years', '-18 years'),
	];
})
->state(Candidate::class, 'with_citizenship', function ($faker) {
	return [
		'citizenship' => collect(['american', 'german'])->random(),
	];
})
->state(Candidate::class, 'with_residence', function ($faker) {
	return [
		'residence' => $faker->address(),
	];
})
->afterMakingState(Candidate::class, 'with_title', function ($candidate, $faker) {
	$candidate->fill([
		'title' => $faker->title($candidate->gender),
	]);
})
->afterMakingState(Candidate::class, 'full', function ($candidate, $faker) {
	$candidate->fill([
		'title' => $faker->title($candidate->gender),
		'birthday' => $faker->dateTimeBetween('-50 years', '-18 years'),
		'citizenship' => collect(['american', 'german'])->random(),
		'residence' => $faker->address(),
	]);
});