<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DictionariesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('d_genders')->insert([
        	['name' => 'male'],
        	['name' => 'female'],
        ]);

        DB::table('d_contact_types')->insert([
        	['name' => 'email'],
        	['name' => 'phone'],
        	['name' => 'site'],
        	['name' => 'linkedin'],
        	['name' => 'glassdoor'],
        ]);

        DB::table('d_countries')->insert([
        	['name' => 'usa', 'citizenship' => 'american'],
        	['name' => 'germany', 'citizenship' => 'german'],
        	['name' => 'england', 'citizenship' => 'english'],
        ]);

        DB::table('d_languages')->insert([
        	['name' => 'english'],
        	['name' => 'german'],
        ]);
    }
}
