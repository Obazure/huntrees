<?php

use Illuminate\Database\Seeder;
use App\Entities\Candidates\Candidate;

class CandidateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (App::environment('local')) {
        	factory(Candidate::class, 10)
        		->state('full')
        		->create();
        }
    }
}
