<?php

use Illuminate\Database\Seeder;
use App\Entities\Candidates\CandidateStatus;

class CandidateStatusSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		if (App::environment('local') || App::environment('testing')) {
			CandidateStatus::create([
				'name' => 'initial',
				'desc' => 'Candidate is not verified',
				'serial' => 1,
			]);

			CandidateStatus::create([
				'name' => 'cv-received',
				'desc' => 'Candidate\'s CV has been received and verified',
				'serial' => 2,
			]);

			CandidateStatus::create([
				'name' => 'interview-passed',
				'desc' => 'Candidate have passed the interview',
				'serial' => 3,
			]);

			CandidateStatus::create([
				'name' => 'verified',
				'desc' => 'Candidate have passed all verification tests.',
				'serial' => 4,
			]);
		}
	}
}
