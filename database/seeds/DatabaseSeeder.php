<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
	/**
	 * Seed the application's database.
	 *
	 * @return void
	 */
	public function run()
	{
		// System user is used, to indicate generated events
		// For example, adding a comment to instance
		$system = factory(\App\User::class)->create([
			'name' => 'system',
			'email' => "system@" . strtolower(config('app.name', 'laravel')) . ".com",
			'password' => Hash::make(Str::random(16)),
		]);

		Auth::login($system);

		$this->call(DictionariesSeeder::class);
		$this->call(CandidateStatusSeeder::class);
		$this->call(CandidateSeeder::class);

		if (App::environment('local')) {
			factory(\App\User::class)->create([
				'email' => "admin@" . strtolower(config('app.name', 'laravel')) . ".com"
			]);
		}

		Auth::logout();
	}
}
