<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCandidatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidates', function (Blueprint $table) {
            $table->id();
            $table->string('status', 16);
            $table->string('title')->nullable();
            $table->string('name');
            $table->string('surname');
            $table->string('gender', 16)->nullable();
            $table->string('citizenship', 16)->nullable();
            $table->string('residence')->nullable();
            $table->date('birthday')->nullable();
            $table->timestamps();

            $table->foreign('status')->references('name')->on('candidate_statuses')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidates');
    }
}
